package uppgift4;
//använder Runnable för interface
public class Prime implements Runnable{
    private final int start;
    private final int slut;

    public Prime(int start, int slut) {
        this.start = start;
        this.slut = slut;
    }

    static boolean primtal(int n) {
        if (n == 1 || n == 0) return false;

        for (int i = 2; i < n; i++) {
            if (n % i == 0) return false;
        }
        return true;

    }

    @Override
    public void run() {
        for (int i = start; i < slut; i++) {
            //använder metoden primtal och i är för loop
            if (primtal(i)){
                System.out.println(Thread.currentThread().getName()+" : " +i);
            }
        }
    }

}

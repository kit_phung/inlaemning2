package uppgift1;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        //använder av mig interface lista som jag importera
        List<Person> person = List.of(
                new Person("Kit", "male", 35000),
                new Person("pavel", "male", 40000),
                new Person("Edin", "male", 30000),
                new Person("Martina", "female", 5),
                new Person("Tina", "female", 20000),
                new Person("Bojan", "male", 15000),
                new Person("karolina", "female", 10000),
                new Person("Erik", "male", 12500),
                new Person("Björn", "male", 23500),
                new Person("Sandra", "female", 12000)
        );

        //använder lista

        //
        Map<String,Double> averageMaleAndFemale=person.stream()
                .collect(Collectors.groupingBy(Person::getGender,Collectors.averagingDouble(Person::getSalary)));
        System.out.println(averageMaleAndFemale);

        System.out.println(person.stream()
                .collect(Collectors.groupingBy(Person::getGender,Collectors.averagingDouble(Person::getSalary))));


        System.out.println(person.stream()
                .max(Comparator.comparing(Person::getSalary)));

        System.out.println(person.stream()
                .min(Comparator.comparing(Person::getSalary)));


    }
}

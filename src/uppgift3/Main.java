package uppgift3;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) {
        // jämförelse
        String regex = "[aAeEiIoOuU].[aAeEiIoOuU]";
        //skapa lista
        List<String> words = new ArrayList<>();

        words.add("ek");
        words.add("träd");
        words.add("apa");
        words.add("ögat");
        words.add("lejon");

        //skapa en ny lista och spara det som word2, använder stream från lista words(ovan)
        List<String> word2 = words.stream()
                //filtera bort  alla som är falsk och spara alla som är sant i word2lista
                .filter(w -> (match(regex, w))).toList();
        //använder word2lista och skriver ut
        word2.forEach(System.out::println);

    }
    public static boolean match(String regex, String word) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(word);
        if (matcher.find()) {
            return true;
        }
        return false;
    }

}

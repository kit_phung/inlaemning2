package uppgift2;

public class CarFactory {
    //Här skapa vi bilar
    public makeCar getMake(String make){
        if(make==null){
            return null;
        }
        if(make.equalsIgnoreCase("TESLA")){
            return new Tesla();
        }
        else if(make.equalsIgnoreCase("BMW")){
            return new Bmw();
        }
        else if(make.equalsIgnoreCase("Mercedes")){
            return new Mercedes();
        }
        return null;

    }
}

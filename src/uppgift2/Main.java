package uppgift2;

public class Main {

    public static void main(String[] args) {
        CarFactory carFactory=new CarFactory();

        makeCar car1= carFactory.getMake("tesla");
        car1.make();
        makeCar car2=carFactory.getMake("Bmw");
        car2.make();
        makeCar car3=carFactory.getMake("Mercedes");
        car3.make();

    }
}
